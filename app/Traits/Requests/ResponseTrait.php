<?php

namespace App\Traits\Requests;

use Illuminate\Support\Facades\DB;

trait ResponseTrait {

    protected function resolve($message, $data, int $statusCode = 200) {
        DB::commit();
        $response = $this->formatResponse(
            $this->resolveMessage($message),
            $data, $statusCode
        );

        return response()->json($response, $statusCode);
    }

    protected function reject(String $errorMessage, ?Array $data = null, $statusCode = 404) {
        DB::rollBack();
        $response = $this->formatResponse(
            $this->resolveMessage($errorMessage, false),
            $data, $statusCode
        );

        return response()->json($response, $statusCode);
    }

    private function resolveMessage($message, $isSuccess = true) {
        return gettype($message) === "string"
        ? [
            'title'   => $isSuccess ? "Success" : "Something went wrong",
            'message' => $message,
        ]
        : $message;
    }

    private function formatResponse($message, $data, int $statusCode) {
        return [ 'message' => $message, 'result'  => $data ];
    }

}