<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BindingServiceProvider extends ServiceProvider {

    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [];


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        foreach ($this->bindings as $request => $requester) {
            $this->bindDependency($request, $requester);
        }
    }

    protected function bindDependency(String $dependency, Array $bindings) {
        foreach ($bindings as $dependent => $service) {
            $this->app
                ->when($dependent)
                ->needs($dependency)
                ->give($service);
        }
    }
}
