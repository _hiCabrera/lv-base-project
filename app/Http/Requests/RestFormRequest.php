<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class RestFormRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return false;
    }

    public function getRules(): Array{
        switch (request()->method()) {
            case 'POST':
                return $this->getPostRules();
            case 'PUT':
                return $this->getPutRules();
            case 'PATCH':
                return $this->getPatchRules();
            case 'DELETE':
                return $this->getDeleteRules();
            default:
                return [];
        }
    }

    protected function getPostRules(): Array {
        return [];
    }

    protected function getPutRules(): Array {
        return [];
    }
    
    protected function getPatchRules(): Array {
        return [];
    }

    protected function getDeleteRules(): Array {
        return [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return $this->getRules();
    }

    public function getFormData(): Array {
        return $this->only($this->getFormKeys());
    }

    public function getFormKeys(): Array {
        switch (request()->method()) {
            case 'POST':
                return $this->getPostKeys();
            case 'PUT':
                return $this->getPutKeys();
            case 'PATCH':
                return $this->getPatchKeys();
            case 'DELETE':
                return $this->getDeleteKeys();
            default:
                return [];
        }
    }

    protected function getPostKeys(): Array {
        return [];
    }

    protected function getPutKeys(): Array {
        return [];
    }

    protected function getPatchKeys(): Array {
        return [];
    }

    protected function getDeleteKeys(): Array {
        return [];
    }
}
