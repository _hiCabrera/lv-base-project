<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function view(String $path, ?Array $data = null) {
        $path = $this->resolvePath($path);

        return $data ? view($path)->with($data) : view($path);
    }

    protected function resolvePath(String $path) {
        $viewPath = property_exists($this, 'viewPath')
            ? $this->viewPath . '.' : "";

        return $viewPath . $path;
    }


}
